Linux is Most Powerful 
Ansible , Kuberrnetes , Jenkins etc all Run well on Linux
Devops Required you to know basic Linux well 

Recommended Books - Phonix Project , Unicorn Project

Linux is Open Source Operating System 
Linux Ubuntu comes with some basic Programs Untility

In Sriparna Laptor WSL Ubuntu - aniket , root both have password - 'password'

Linux GUI is Limited functionality so we need to learn shell

shell - a Program to interact with Linux OS 

Logged in Default Dir - /home/aniket Dir 
Each user have own Home Dir - To store their personal Data and Configs  - Dedicated lOCKER TO YOU 
Other user cannot access it 
 ` == Repesent Home Dir 

Some commands need argument and Some work without argument
echo aniket
uptime
echo -n hello = No new Line will print


Commands can be two types 
Part of the shell - cd , pwd , cd , ls 

Other External Programs - Preinstalled , Installed later 

type cd ===  type command will tell you about nature of the command 

/ == Root Dir 
Absolute Path - Provide Complete Path from Root i.e /
Relative Path --- Path in relation to Present Working Dir 

pushd / popd  - use stack up the Dir change actuions and Pop them later

mv == This command we can use File , Dir from One place to another 
cp == Copy Operation
-r Flag == Reecursive Operation
rm == Remove /Delete Operation 
cat == This Command Print Data of File 
cat > Sample.txt 
Give some Lines 
Crlt + D 
Or 
cat City.txt > City2.txt

more ==Command to Traverse the File 
less == Command to Traver the File Contest 

. === Current Dir 
.. === Parent Dir 

help == Flag  for Help manual 
date --help


**************************
Shell Types 
Bourne Shell 
C Shell 
Korn Shell 
Z shell 
Bourne Again Shell 

echo $SHELL  -- Current shell type 

Bash is the most common shell 
Give auto complete 
Bash can give custom alias to command 
alias dt = date 
history - this command gives list of prev Run shell 

$SHELL == this is a Env Variable 

env == Command to see all env vars in system 
export == command to set new env variable 

 export myname=ANIKET
  echo $myname -- Will print ANIKET

Change shell using command --- chsh


 export myname=ANIKET == persist only to this Login
 myname=ANIKET === live only to current shell 
 To persist over login logout add them to profile  [`/.profile or `/.pam_environment]

echo $PATH === Path variable 
which vi === Path Variable is Set or Not 

 export PATH=$PATH:/opt/obs/bin -- Set Path variable 

 You can Customise Bash Prompt also 

echo $PS1

******** LINUX KERNEL ------------------

Linux Kernal Duties 
Memory Manangment - Keep track of how much memory is being used 
Process Management - CPU running which process and for how long , Scheduling the processes for cpu cycle 
Device Driver - Translate Hanrdware Instructions to Applications 
System Calls and Security - Receve request from services 

Kernel is Monolithin and Modular to extend capabilities


uname -r === Command to name of Kernel

kernel.org - have full details 

Memory is devided into Two spaces 

Kernel Space --- Kernel Process , Device Driver [Unrestricted access to HW and OS ]  
User Space ---- User  Application Process [have limited access to System , OS , Hardware ]

Apps running on User Space ---> Do system call to --> Kernal Space Apps Like Devide Driver  --> Access HW 

---- HOW HARDWARES WORKS WITH LINUX 

The momneent Some hardware attached --- Kernal Space Device driver send --> uevents --> To user space app [udev] --- A dir created -- /dev/sdb1

dmesg == this is used to get messages from hardware that kernel detect 

 dmesg  | grep -i pci == Get all logs of pci related hardwatres 

 udevadm == This Monitor listen to the Attached hardware and Print its Info and Events 

 udevadm info --query=path --name=/dev/sda3

lspci == Periphirial Component Integration Lists in the System 

lsblk - List down all the Storage Disks 

lscpu === CPU information command 

32 Bit Computer can Map upto 4 GB Ram 
64 Bit Compouter Can have Large Ram 

32 Bit can Run 32 Bit OS and Instructions 
64 Bit is compatiable for Both 

lsmeme --summary  == Tell About Ram 

free -m /-g / -k  == Show free memory in system 

lshw == tell full ssystem info 

Some Command needs Root privilage 
you can use sudo in front of command 

sudo lshw 

****LINUX BOOT SEQUENCE ***************
BIOS POST  --- Power on self test -- basic hardware check 

Boot loader (GRUB2 - GRAND UNIFIED BOIOT LOADER V2 ) --- Bios load boot code from hw Boot Section [May give some option to user ] Availablre in /boot 

Kernal Init - Kernel load to Memory and Start execution - Initialize the processes , Then Start init process --- and set up User space and start Init POricess [systemd]

Service Inti  == systemd --> mount Files and Make system available to user 

ls -ltr /sbin/init
lrwxrwxrwx 1 root root 20 Jul  9  2020 /sbin/init -> /lib/systemd/systemd

systemd target you can configure to Run on various Modes When system Boots Up 
Runlevel 5 - Graphical Modes 
Runlevel 3 - Command Line  Mode 

This are called Runlevel systemctl get-defaultas

 systemctl get-default == Provide the Current runlevel and systemd Targets 

graphical.target

 ls -ltr /etc/systemd/system/default.target --- Give you current setup 

sudo systemctl set-default multi-user.target
 systemctl get-default

 The term runlevels is used in the sysV init systems. These have been replaced by systemd targets in systemd based systems.

The complete list of runlevels and the corresponding systemd targets can be seen below:

runlevel 0 -> poweroff.target

runlevel 1 -> rescue.target

runlevel 2 -> multi-user.target

runlevel 3 -> multi-user.target

runlevel 4 -> multi-user.target

runlevel 5 -> graphical.target

runlevel 6 -> reboot.target


systemd is faster boot than systV init


**** FILE TYPES IN LINUX *********** 
Every object on Linux is File 
Regular File - contains Binary Data / Txtx /Image 
Dir --- Conainer of Other file and Dir
Spacial Files --all in /dev 
Character Files --- Allow os to communicate with Mosue , Keyboard  
Block Files  -  Read /write Data in Blocks 
Link Files --- 

Hard Link - Actual Point 
Soft Link - Deletion of actual data not affect this 
Socket Files 
Named Pipes - Unidirection Data Flow 

file xyz.txt --- tell file type 
ls -ltr 

crw-rw-rw-
lrwxrwxrwx
drwxr-xr-x

c , l ,d -- they are all file type 

File system Hirarchu --- 
/ == root  ==>Root user Home Dir 
/home === >Root dir of other user 


drwx------   2 root root   16384 Apr 10  2019 lost+found
drwxr-xr-x   2 root root    4096 Jul 10  2020 snap
drwxr-xr-x   2 root root    4096 Aug 21  2020 srv
drwxr-xr-x   2 root root    4096 Aug 21  2020 opt === Here external sw are installed 
drwxr-xr-x   2 root root    4096 Aug 21  2020 media === all external Disks Mounted here like pendrive [df command can be used ]
drwxr-xr-x  10 root root    4096 Aug 21  2020 usr ==== all user apps and  data store here [Firefox , vi , Mail client ] 
drwxr-xr-x   2 root root    4096 Aug 21  2020 lib64 ---- shared Libs stored here 
drwxr-xr-x  20 root root    4096 Aug 21  2020 lib --- same as above 
drwxr-xr-x  13 root root    4096 Aug 21  2020 var == store Logs systemn wide 
drwxr-xr-x   2 root root    4096 Aug 21  2020 boot
-rwxr-xr-x   3 root root 1392928 Mar  5 12:30 init
drwxr-xr-x   2 root root    4096 Apr 30 20:28 bin === Store commands Def 
drwxr-xr-x   2 root root    4096 Apr 30 20:28 sbin
drwxr-xr-x   4 root root    4096 Apr 30 20:28 mnt ==== to Mount temporary Mount Filesystem 
drwxr-xr-x   5 root root    4096 Jun  9 02:14 home
drwx------   3 root root    4096 Jul  1 20:51 root
dr-xr-xr-x  11 root root       0 Jul  2 09:57 sys
drwxr-xr-x   8 root root    2840 Jul  2 09:57 dev == Special Files and Stored here 
dr-xr-xr-x 294 root root       0 Jul  2 09:57 proc
drwxr-xr-x  92 root root    4096 Jul  2 09:57 etc == Most config files stored here 
drwxr-xr-x   7 root root     200 Jul  2 10:20 run
drwxrwxrwt   4 root root    4096 Jul  2 10:35 tmp === to store temporary data 



********* LINUX PACKAGE MANAGEMENT **********************
DPKG / APT  - UBUNTU /DEBIAN 
RPM -- REDHAT , CENTOS 

There are many linux dists available 
they are categoised based on package manager 

REDHAT - Paid enterpeise server 
CENTOS - Feww Enterprise Server 

Software package --- Compressed archive of all necessary files to run the Sw 

Package manager make sure all dependencies also get installed for the current package 

Package manager  is a software to give consustest way to install , upgrade , uninstall , check integrity , manage dependency  etc 

Base  : DPKG --- DEBIAN PACKAGE MANAGER [DO NOT HANDLE DEPENDENCY ]
Front end :  APT ----ADVANCED PACKAGE MANAGER - BETTER  [AUTOMATICALLY HANDLE DEPENDENCY ]
Enhanced command : APT-GET ---- [AUTOMATICALLY HANDLE DEPENDENCY ]

file extension deb
dpkg -i  telnet.deb -- Install
dpkg -r  telnet.deb - Unisntall 
dpkg -l  telnet.deb -- List 
dpkg -s  telnet.deb -- Status 
dpkg -v  telnet.deb -- Verify 

apt rely on dpkg 
apt depende on software repos 
/etc/apt/sources.list  --Here the Repo info are stored

apt update -- refresh repos 
apt upgrade  -- Upgrade all installed packages 
apt edit-sources 
Will Open /etc/apt/sources.list in vi editor 

apt install telnet 
apt remove telnet 
apt search telnet 
apt list  | grep telnet 

************************
apt  vs apt-get 

apt is easy on eye 
Give progress bar 
give outout in user friendly way 
search is friedly  --apt-get do not have sear need to use apt-cache [tthat also not user friendly ]

*************************

Base  : RPM ---  REDHAT PACKAGE MANAGER  [DO NOT HANDLE DEPENDENCY ]
Front end :  YUM ---- AUTOMATICALLY HANDLE DEPENDENCIES 
Enhanced command : DNF ----

rpm -i  telnet.rpm

File extensiom rpm 
install  rpm -ivh telnet.rpm
uninstall rpm -e telnet.rpm
update rpm -Uvh telnet.rpm
query  rpm -q telnet.rpm
verify  rpm -V telnet.rpm === To ensure downloaded from trusted store 

yum internally use rpm 
yum depend on sw repositories 
/etc/yum.repo.d  --Here the Repo info are stored 
OS comes with own default Repo pre comnfigured 
You can add addition Repos als o here 

yum install httpd 
yum install -y httpd 
Check if already installed 
check Repo Info 
Check dependencies 
Ask poermission 
Install 

yum repolist 

yum provides scp 

yum remove httpd

yum update telnet 

yum update == update all packlages 

*****************************************
Apart from ls -ltr we can use du command to check File size 

du -sh dir/file 
1.1M    aniket.tar

----- ARCHIVING FILE ------------------
tar command can be used to operate on archive File - tarball 


tar -cf aniket.tar file2 file2 dir1 dir2 //Create archieve 
tar -tf aniket.tar //Check files in archieve 
tar -xf aniket.tar // Unpac File 
tar -zcf aniket.tar file2 file2 dir1 dir2 //Create archieve [Compressed ]

Three major type of Compression 

bzip2 image.img ---> image.img.bz
bunzip2 image.img.bz ---> image.img 

gzip image.img ---> image.img.gz 
gunzip image.img.gz ---> image.img

xz  image.img----> image.img.xz 
unxz  image.img.xz----> image.img

We need not to uncompress a File in above formats to read conente
you can use bzcat / zcat / xzcat to read them 


****************** SEARCH FILES AND DIR IN SYSTEM ********************
locate command can be used  - it use mlocate cache db - so will take time -- locate City.txt
updatedb command can be used to update db cache [/var/lib/mlocate/mlocate.db]

find command can be used also to find files  -- [find /home -name City.txt ] 


------- GREP Command ----------------------
Grep is a poweful command to do patten search on file content 
grep command is case sensitive 

grep second sample.txt --- Show lines where matching eywrod find 
grep -i second sample.txt --- Case insensitive search 
grep -v second sample.txt -- Print lines which do not contains the keyword 
grep -w second sample.txt -- Print lines which do  contains the keyword Whole word 
grep -r "some text" /target-dir ----> Search for all files in target dir for the file content and list down files where this line exists
grep -A1 anik sample.txt -- Print Matching line and One line below 
grep -B2 anik sample.txt -- Print Matching line and Two  line above  

grep -A1 -B4   anik sample.txt //Is a valid command also

--------------------IO REDIRECTION - -----------------

There are total 3 Kind of IO Stream 
Standard Input - take input from keyboard , Mouse 
Standard Output - Show data in console 
Standard Error - Shpw weeror via this 

This streams can be re directed to text file instead of printing 

 sudo echo $PATH > sample.txt //Oveerite File content 
  sudo echo $PATH >> sample.txt // Append to the Last 

Redirect error method 

cat aaa 2> sample.txt //Overwirte content of file with stderr content insted of printing in console
cat aaa 2>> sample.txt //append content of file with stderr content insted of printing in console

cat miising_file 2> /dev/null == Dump data to a place which we do not need 


*** COMMAND LINE PIPELS *******************
Command line pipes help to link multiple commands one after another 
command1 | command 2 

 cat sample.txt
Hello world 
I am aniket roy
aniket is a good boy

grep aniket sample.txt | grep is

Output : aniket is a good boy

---- TEE COMMAND -------
echo "aaa" >> sample.txt
echo  "aaa" | tee -a sample.txt //Here the difference it is also printing to console as well as write to file [-a == append ]


-VI EDITOR ---- WORKS ON DIFFERENT MODE 
VIM -- VI IMPROVED 
In most common linux dist vi is just an alias of Vim 

root@Sriparna:/# which vi
/usr/bin/vi
root@Sriparna:/# ls -ltr /usr/bin/vi
lrwxrwxrwx 1 root root 20 Aug 21  2020 /usr/bin/vi -> /etc/alternatives/vi
root@Sriparna:/# ls -ltr /etc/alternatives/vi
lrwxrwxrwx 1 root root 18 Aug 21  2020 /etc/alternatives/vi -> /usr/bin/vim.basic


VIM /VI OPERATIONS -------

I == insert Mode 
esc - Back 
: -- last line 
:q --- quit 
:w 
:q! --- Quit without confirmation 
y -- copy 
p -- paste 
x - delete a lteer 
dd - delete a li ne 
delete 3 lines --- d3d 
u -- undo 
crtl + r --- redo 
/aniket --- find word aniket 
?aniket --- find word buttom to top 
n , N to go to next /prev occurance 

********************* AWK and SED Command ********************
cat awksample.txt
aa bb cc 
dd ee ff 
gg hh jj
ii uu yy 

awk use space as default delimiter 
awk '{print}' awksample.txt / awk '{print $0}' awksample.txt
aa bb cc 
dd ee ff
gg hh jj
ii uu yy
awk '{print $1}' awksample.txt
aa
dd
gg
ii
 awk '{print $NF}' awksample.txt
 cc
ff
jj
yy
awk '{print $1 , $NF}' awksample.txt
aa cc
dd ff
gg jj
ii yy
ls -l | awk '{print $1 , $NF}' -- Piping examp[le ]
-rw-r--r-- awksample.txt
drwxr-xr-x bin
drwxr-xr-x boot
drwxr-xr-x dev
drwxr-xr-x etc
drwxr-xr-x home
-rw-r--r-- ing_file
-rwxr-xr-x init
drwxr-xr-x lib

cat  awksample.txt
aniket/roy:is:good
anikt:roy
data
//-F can be used to specify defferent Delimiter othet than space[default]
awk -F ':' '{print $NF}' awksample.txt
good
roy
data

*********************************************
cat sedexample.txt 
biriyani kabab sriparna
aniket roy sriparna
data roy kabab

sed 's/kabab/charcole/' sedexample.txt == Just print on console 
sed -i 's/kabab/charcole/' sedexample.txt

When / is there in search Keyword better use different delimiter 
sed -i 's.kabab.charcole.' sedexample.txt

***Piping using SED ******
echo "Hello World" | sed 's/World/aniket/'
Hello aniket



********** Storage System in Linux *****************
Block Device -- find in /dev location ---- Data read / written in block 

lsblk

NAME  MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0   7:0    0 349.4M  1 loop /mnt/wsl/docker-desktop/cli-tools
loop1   7:1    0 185.3M  1 loop
sda     8:0    0   256G  0 disk
sdb     8:16   0   256G  0 disk /mnt/wsl/docker-desktop/docker-desktop-user-distro
sdc     8:32   0   256G  0 disk /mnt/wsl/docker-desktop-data/version-pack-data
sdd     8:48   0   256G  0 disk /

sd == Physical Disk 
Major Number - Device type -- RAM / HAD DISK 
Minor Number - Inidivisual Physical/logical partition 
7:0
7:1
Or 
 ls -ltr /dev | grep "^b" | grep "sd*"


----- Partition -----------------
Strorage Disk is Devided into multiple partition 
each partition used for various purpose 

we shoud partition disk for greater flexibility 

fdisk -l /dev/sda

Disk /dev/sda: 256 GiB, 274877906944 bytes, 536870912 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes

partitio Types - 

Max size of Disk - 2 TB 
Primary Patition [4 Max ] - Store OS 
Extended Partition - Can Hoast Logical Partition 
Is has Partition table to sotre partition info  - MBR Partitioning Scheme - Master Boot Record 
Logical Partition - Store inside extended Partition

gpt - GUID Partition table - Enhanced mechanism of MBR 
No Partition Limit 
No max size disk Limit 


gdisk /dev/sdb  === [/dev/sdb -- Disk Path ] -This command can be used to create Partition 

Linux Filesystem ---------------
After partitionng the Partition is seen as Raw Partition BY KERNEL 
It cannot be used directly 
1.Create File system 
2.Mount Filesystem to a Dir 
3.Then use it 

-Commaonly File Systems 
ex2 , ex3 , ex4 

ex2 , ext3 - Max 2 TB File size 
Max Volume size 4 TB 

when system crash - ext2 take Long time to startup again 
ext3 - Then time post system crash is reduced 

ext4 - Most recent and advance File system 
Max file size 16 TB 
1 Exabyte - Volume size 
backward compatiable 
use journal , checksum 


#Create File system 
mkfs.ext4 /dev/sdb1 [/dev/sdb1 == Partition path]
#Mount to a Dir 
mkdir mnt/ext4
mount /dev/sdb1  mnt/ext4

Use mount command to verify action 

mount | grep /dev/sdb1

To make the Filesystem available add entry to 
/etc/fstab 

echo "/dev/sdb1 mnt/ext4 ext4 rw  0 0 " >> /etc/fstab

***** NCOMMONLY USED EXTERNAL STORAGE *********
For desktop we can use internal Storeage 
** In enterpeise server - We need to use entrprise grade high capacity high available Storage 

BLOCK DEVICES ------

DAS - Direct attached Storage - Directly connected via prephiripal - No firewall , Network / Fast / Cheap / Single Server / High chance for faliure / Good for small Business /Kernel see it as Block storeage 
NAS - Netwrok atached Storage  - Connected via Network  / Slow / Kernel see it as Directory / Good for crntralised Shared storage /Backend Storage for Webserver /Nas shoud not be used as OS drive 
SAN - Storage arear Network - Pool of Sotrage - Kernel will dtect it as Raw Disk - we can now partition and mount file system / High Speed fiber used /Fiber channel switch is used /Good for database 

NETWORK STORAGE -----

NFS - network File system  - Directory shared via Network to multiple people / Good for storing Sw repos  
/etc/exports -- here you need to configure NFS access 
exportfs -a //export to all clients 
exportfs -o //to export to indivisdual clients
then you need to use "mount" command to mont them to all allowed clients

#Check Doc kodekloud 


-------------- LVM -------------
logical Volume Manager  - allows grouping of multiple Physical Volumnes[Hard disks] to unlimited Logical Volumes 
Easy to create and expand as wish

PHysically we devide a Volume or Hard disk to Physical Partitions 

Logical Volume can range over Multiple Physical Partitions - Its a virtual layer for better storage management

sudo apt-get install lvm2 -- tool to be installed 
First create Physical Volume on Disk 
pvcreate /dev/sdb 
Second create Volume Group on top of Physical Volume 
vgcreate vg_1 /dev/sdb
Thrid crrate Logical Volume on top of Volume Group
lvcreate -L 1G -n log_vol_1 vg_1

pvdisplay
vgdisplay
lvdisplay
lvs

Fourth Create File system 

#Create File system 
mkfs.ext4 /dev/vg_1/log_vol_1 
#Mount to a Dir 
mkdir mnt/vol1
mount /dev/vg_1/log_vol_1  mnt/vol1


---- Resizing the Volume -----
vgs -- check space 
lvresize -L +1G -n /dev/vg_1/log_vol_1 === Only Volume is resized /Now filesystem also need to be resied
resize2fs /dev/vg_1/log_vol_1 ==this will resize File system 

all this can be done on running system where file system is already in use 

check status 

df -hP /mnt/vol1

******************** SYSTEM MANAGEMNT IN LINUX *******************
SYSTEMD - service management 
--Need to make a Script Start at Start and have dependency on Posgres service running 
--Restart service automatically 
-- Service can start / stop manually - shoud not restart 

Create a Unti File in 

/etc/systemd/system this Dir 
my-prject.service 


[Unit]
Description=This is a sample Service 
Documentation=Some link to Doc can be added here 
After=postgresql.service
[Service]
ExecStart= /bin/bash /usr/bin/my-service.sh

User=project_user
Restart=on-faliure
RestartSec=10

[Install]
WantedBy graphical.target

systemctl demon-reload  // This command reload system manager config - and aware of all changes
systemctl start my-prject.service 

//Manula intervention 

systemctl start my-prject.service 
systemctl status my-prject.service 
systemctl stop my-prject.service 


Additional Monitoring Tools ----------------

systemctl -- tool to manage services 
journalctl -- Tool to debug system 

systemctl start /stop/reload/enable/disable/restart/status docker 

systemctl edit project-merc.service --full ==Now you can change and Immediately they will affect without demon reload
systemctl get-default ==== Current Run level 
systemctl set-default multi-user.target

systemctl list-units --all // All running Units in the System 

journalctl  -- all Logs of service show 

journal -b -- All logs of current Boot 

journlactl -u docker.service --- All logs of Docker service 

****************************************************************
******************** NETWORK IN LINUX **************************
****************************************************************

ping 192.169.1.0

ping database  --Will not work Now 

/etc/hosts === Common DNS Storage for a Linux System 
cat >> /etc/hosts 
192.168.1.0 database

ping database -- Will work Now in your system

ping / curl / ssh -- all loogs to /etc/hosts file 
nslookup is also good command 

Instead of managing each hosts file in all systems better to have a DNS Server where you maintain them 
then Point all systems to there for DNS resolution

DNS == 192.169.1.100 [DNS Server IP ]


In all Machines Do this [this file is called DNS Config File ]
cat >> /etc/resolv.conf 
nameserver 192.169.1.100

You can still use your hosts file to store your sepcific requirements 

If Both Entry is there in hosts and  resolv.conf then ---Host is check first /if not then find in dns server 

cat /etc/nsswitch.conf -- here you can change the order 

hosts:          files dns
# You can make changes here 

#If the entry is Not found in both DNS SERVER , AS WELL AS IN HOSTS THEN ? 
It will fail -- you can have multiple nameserver to check further 

nameserver 8.8.8.8 ---- this is very popular and hosted by google 


----- DOMAIN NAMES ---------------------
.com --- commercial --root domain 
google -- top level Google Domain 

mail , drive , www etc are subdoimain in google
First request to to Org dns Server --- Root DNS --- .com DNS ---- Google DNS --- Get ip 
Org dns generally cache this traver for some time to spped up IP resilution

How to configure web --> web.mycompany.name [To be used intenally in my Company network ]

In your machine host file 

cat >> /etc/resolv.conf 
search   mycomapny.com 


Now Ping web actually ping web.mycompany.com 


***** RECORD TYPE *******

A Record --- IP and DNS Name 

web-server 192.168.2.3

AAAA Record --- IPV6 and DNS Mapping 

web-server 2a03:2880:f137:182:face:b00c:0:25de

CNAME Record --- Maping One Hostname to multiple names 

food.web-server  eat.web-server, hungry.web-server [Used for alias naming ]

nslookup -- Do not consider local Hosts File 

nslookup www.facebook.com
Server:         172.24.176.1
Address:        172.24.176.1#53

Non-authoritative answer:
www.facebook.com        canonical name = star-mini.c10r.facebook.com.
Name:   star-mini.c10r.facebook.com
Address: 157.240.192.35
Name:   star-mini.c10r.facebook.com
Address: 2a03:2880:f137:182:face:b00c:0:25de


dig www.facebook.com

; <<>> DiG 9.11.3-1ubuntu1.13-Ubuntu <<>> www.facebook.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 32140
;; flags: qr rd ad; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 0
;; WARNING: recursion requested but not available

;; QUESTION SECTION:
;www.facebook.com.              IN      A

;; ANSWER SECTION:
www.facebook.com.       0       IN      CNAME   star-mini.c10r.facebook.com.
star-mini.c10r.facebook.com. 0  IN      A       157.240.192.35

;; Query time: 3 msec
;; SERVER: 172.24.176.1#53(172.24.176.1)
;; WHEN: Mon Jul 04 12:08:25 IST 2022
;; MSG SIZE  rcvd: 134


------ NETWORKING BASICS -------------------

Switch create a network between two Hosts 

ip link -- Provide host machine network interface 

suppose switch address - 192.168.2.0

Two machinjes connected to the switch 
ip addr add 192.168.2.1/24 dev ethh0 
ip addr add 192.168.2.2/24 dev ethh0 

ip addr - List ips assigned to eth interfaces 

System from One network Connect to system to other network via Router 
Router --- Connect many small networks
Router assign Ip TO EACH NETWORKS 

route / ip route
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         Sriparna        0.0.0.0         UG    0      0        0 eth0
172.24.176.0    0.0.0.0         255.255.240.0   U     0      0        0 eth0


ip route add  192.168.2.0/24 via 172.156.0.1 --Here we are assigning the Gateway from Swtch to Router 

Gateway is the Door from where One Switch communicate to a Router [Need to setup on each switch]

Gateway need to be configured for all Switches 

Now you can connect Router to the internwt 
then you can make a default entry to reach internet

To persust change over restart add the configs in /etc/networks/interfacts File  

sudo apt install inetutils-traceroute
sudo apt install traceroute

traceroute 192.168.0.1 -- Show Hops via network devices and Problems 

netstat -an  //Show active Pors 

ip link set dev  etho0 up // To start network interface 

*********************** SECURITY AND ACCESS CONTROL ON LINUX *******************
Access Control --- user/id password 
PAM - probable Auth Method - for apps Access control 
Network Security - Linux OS nwk Security 
SSH Hardning - Access secure server via Unsecured network 

Accounts in Linux ----------------------

Every user have a associated to an account 
it store user id , password , UID ,GID 
cat /etc/passwd


aniket:x:1000:1000:,,,:/home/aniket:/bin/bash

# Here we can see user Home Dir as well as default shell assigned to the user -- above outpout 

Group - Group od User account based on common attributes / operations
User can be part of multiple group - If during user creation no groups mentioned - User UID === GID 
cat /etc/group

adm:x:4:syslog,aniket
dialout:x:20:aniket
cdrom:x:24:aniket
floppy:x:25:aniket
sudo:x:27:aniket
audio:x:29:aniket
dip:x:30:aniket
video:x:44:aniket
plugdev:x:46:aniket
netdev:x:114:aniket
aniket:x:1000:
docker:x:1001:aniket

Each group have unique id - GID

All User , Group Info can be found 

id aniket -- this command

uid=1000(aniket) gid=1000(aniket) groups=1000(aniket),4(adm),20(dialout),24(cdrom),25(floppy),27(sudo),29(audio),30(dip),44(video),46(plugdev),114(netdev),1001(docker)


---- ACCOUNT TYPES ------
User Account -Invidual User account 
Super User Account - UID - 0 === Unrestricted access 
System Account - Used by Sofwares --- UID --- 100S , 500-100 - They do not have Home Dir 
Service Accounts - Created when Service is cread on System 

Useful Command 
id -- user info 
who -- logged in user info -currently 
last --- logged in user info - past records 


-SWITCH USER -------
sudo is better to use 
su - == this command will need password of the target user 
su - aniket --- switch to aniket 
su -  ---- Shwitch to root user 
sudo - this command give trusted user temporary access to higher root privilage based on their own password 
Need to configure in /etc/sudoers  --Only listed user here can run sudo command 

# Host alias specification

# User alias specification

# Cmnd alias specification

# User privilege specification
root    ALL=(ALL:ALL) ALL

# Members of the admin group may gain root privileges
%admin ALL=(ALL) ALL

# Allow members of group sudo to execute any command
%sudo   ALL=(ALL:ALL) ALL

# See sudoers(5) for more information on "#include" directives:

#includedir /etc/sudoers.d


*************************************************
If we have setup the sudoers access to certain user then we can disable root login completely.
Then No one can login in root user ever - they have to use sudo command only 

cat /etc/passwd | grep root
root:x:0:0:root:/root:/bin/bash

We can change above line like this 

root:x:0:0:root:/root:/usr/sbin/nologin


*************************
 sudo cat /etc/sudoers | grep -v "#" | awk '{print $1}' --Give Hosts 

 ********* USER MANAGEMENT --------------

 aniket@Sriparna:~$ su -
Password: 
root@Sriparna:~# useradd bob
root@Sriparna:~# passwd bob
Enter new UNIX password: 
Retype new UNIX password:
passwd: password updated successfully
root@Sriparna:~#

root@Sriparna:~# id bob
uid=1003(bob) gid=1004(bob) groups=1004(bob)

you csn use some options also with useradd -- check Help 

userdel bob -- delete user 
groupadd developers  ---- add group 
groupdel developers  --- delete group 


-------- ACCESS CONTROL -------------------
/etc - have all access control info - all user can read it , Root can only modify it 
/etc/passwd - Sore User info , default shell , home dir , uid , gid 

USERNAME:PASSWORD:UID:GID:GECOS:HOMEDIR:SHELL

PASSWORD- X -- Just for reference 
GECOS - Geographical Stuff - Optional - Comma Seperated infos 

/etc/shadow - actual password 
USERNAME:PASSWORD:LASTCHANGED:MINAGE:MAXAGE:WARN:INACTIVE:EXPDATE
 epochs - Days after 1 Jan , 1970
LASTCHANGED - when last password changed 
MINAGE /MAXAGE- -- min/max days you have to wait/hate to change passwd 
WARN - from when passwd change warning to show 
INACTIVE - Number of days after passwd is expired ater which it will not allow to login 
EXPIRE - when account expire \

/etc/group  - Group Infos 

NAME:PASSWORD:GID:MEMBERS


** FILE PERMISSION -----------
drwxrwxr-x 2 aniket aniket 4096 Jul  1 18:54 Europe
x sss yyy zzz 

x -- File type 

[Read Write Execute] = sss/yyy/zzz
sss - User Permission  
yyy - Group Permission
zzz - Other permission

r == read  - 4 (octal)
w == write  - 2 
x == execute - 1
- == No Permission  - 0 
Octal Representation 
rw- = 4+2+0 = 6
rwx = 7 = 4+2+1
... So on ..

#IMPORTANT 
Suppose a User do not have permission But the user part of Group which have access - Still user will not be able to access/operate as Linux give user privilage above group privilage

Change permission of Files ----- 

chmod <permission> <filepath>

chmod 777 abcd.txt
chmod 000 abcd.txt
chmod 750 abcd.txt
chmod u+rwx ancd.txt
chmod g+r-x,o+--x abcd.txt

chmod ugo+r-- abcd.txt //Give read access to user , other , group
chmod ugo-r-- abcd.txt //Remove read access from  user , other , group

chmod == this command is used majorly to chasnge File permission 
We can change ownership of a file chown/chgrp 
chown owner:group file 

chown bob:developer abcd.txt //Change ownership of the File to bob user and developer group 
chown bob abcd.txt //Change ownership of the File to bob user 
chgrp developer abcd.txt  //Change ownership of the File to developer group 

